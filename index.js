const SvnVersionInfo = require('./lib/svn-version-info').SvnVersionInfo;

module.exports = { getSvnVersionInfo };


function getSvnVersionInfo(svnPath) {
	return new SvnVersionInfo(svnPath);
}